# Without Force parameter, Update-Help runs only once in each 24-hour period, downloads are limited to 1 GB of uncompressed content per module and help files are installed only when they are newer than the files on the computer.
# The once-per-day limit protects the servers that host the help files and makes it practical for you to add an Update-Help command to your Windows PowerShell profile without incurring the resource cost of repeated connections or downloads.
# https://social.technet.microsoft.com/Forums/en-US/88092154-1096-4010-a2a1-69bfd938231c/question-about-powershell-40?forum=w8itprogeneral
Update-Help

# Remove the current location from the default prompt.
# Use Get-Location if you need to know where you are.
function prompt { "PS> " }
